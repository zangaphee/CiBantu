#!/usr/bin/python3

# Simple Bantu Word Translator
#
# This simple program translates a word from a source language to a target
# language using Swadesh lists of a couple of hundred words from each lexicon.
# Source and target languages must be given on the command line  using ISO 639
# three-letter language codes.
# 
# Usage: refer to the README file in this same directory.
#

import sys, getopt
from nltk.corpus import swadesh

def main(argv):
    unixOptions = "hs:t:w:"
    gnuOptions = ["help", "slang=", "tlang=", "word="]
    try:
        args, vals = getopt.getopt(argv, unixOptions, gnuOptions)
    except getopt.error as err:  
        # output error, and return with an error code
        print (str(err))
        sys.exit(2)
    for arg, val in args:
        if arg in ("-h", "--help"):
            print ("Usage: bantu-simple-trans.py -s <source-language> -t <target-language>")
        elif arg in ("-s", "--slang"):
            srcval = val
            if val == 'nya':
                src = 'ciNyanja'
            elif val == 'swa':
                src = 'kiSwahili'
            elif val == 'yao':
                src = 'ciYawo'
            else:
                print ("Source language not supported.")
                sys.exit(1)
        elif arg in ("-t", "--tlang"):
            s2t = swadesh.entries([srcval, val])
            if val == 'nya':
                tgt = 'ciNyanja'
            elif val == 'swa':
                tgt = 'kiSwahili'
            elif val == 'yao':
                tgt = 'ciYawo'
            else:
                print ("Target language not supported.")
                sys.exit(1)
        elif arg in ("-w", "--word"):
            translate = dict(s2t)
            # need to capture the possible error in the following line: KeyError if translate[val] is empty
            print (val, "in ", tgt, "is:", translate[val])

if __name__ == "__main__":
    main(sys.argv[1:])

